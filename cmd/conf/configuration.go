package conf

import "errors"

const (
	AddressEnv   string = "ADDRESS"
	DBAddressEnv string = "DB_ADDRESS"
)

var (
	ErrNoAddress   = errors.New("no address in configuration")
	ErrNoDBAddress = errors.New("no database address in configuration")
)

type Configuration struct {
	Address   string
	DBAddress string
}

func NewConfiguration(address string, dbAddress string) (*Configuration, error) {
	if address == "" {
		return nil, ErrNoAddress
	}
	if dbAddress == "" {
		return nil, ErrNoDBAddress
	}
	return &Configuration{
		Address:   address,
		DBAddress: dbAddress,
	}, nil
}
