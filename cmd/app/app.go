package app

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/urfave/cli/v2"
	"gitlab.com/test8930941/db-server/cmd/conf"
	"gitlab.com/test8930941/db-server/internal/data"
	"gitlab.com/test8930941/db-server/internal/delivery"
)

const (
	FlagAddress   = "address"
	FlagDBAddress = "db_address"
)

type Application struct {
	cli.App

	Configuration *conf.Configuration
}

func NewApplication() *Application {
	inst := &Application{}

	inst.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:    FlagAddress,
			Aliases: []string{"a"},
			Usage:   "Define working address",
			EnvVars: []string{conf.AddressEnv},
		},
		&cli.StringFlag{
			Name:    FlagDBAddress,
			Aliases: []string{"db"},
			Usage:   "Define db connection address",
			EnvVars: []string{conf.DBAddressEnv},
		},
	}

	inst.Action = inst.run()

	inst.Before = func(c *cli.Context) error {
		config, err := conf.NewConfiguration(c.String(FlagAddress), c.String(FlagDBAddress))
		if err != nil {
			return err
		}

		inst.Configuration = config
		return nil
	}

	return inst
}

func (inst *Application) run() func(c *cli.Context) error {
	return func(c *cli.Context) error {
		// собираем всё необходимое для запуска
		db, err := sqlx.Open("mysql", fmt.Sprintf("%s?parseTime=true", inst.Configuration.DBAddress))
		if err != nil {
			log.Println("service error:", err.Error())
			return err
		}
		repository := data.NewSQLRepository(db)
		service := delivery.NewService(inst.Configuration.Address, repository)

		go func() {
			if err := service.Run(); err != nil {
				log.Println("service error:", err.Error())
			}
		}()

		stopChan := make(chan os.Signal, 1)
		signal.Notify(stopChan, syscall.SIGINT, syscall.SIGTERM)
		<-stopChan

		service.Stop()

		// Пауза для graceful shutdown
		time.Sleep(2 * time.Second)
		return nil
	}
}
