package entities

import (
	"time"

	"github.com/gofrs/uuid"
)

type User struct {
	ID       uuid.UUID `db:"id"`
	Name     string    `db:"name"`
	Phone    string    `db:"phone"`
	Birthday time.Time `db:"birthday"`
}

type UserCreation struct {
	Name     string
	Phone    string
	Birthday time.Time
}

type UserModification struct {
	ID       uuid.UUID
	Name     string
	Phone    string
	Birthday time.Time
}
