package delivery

import (
	"context"

	"github.com/gofrs/uuid"
	"gitlab.com/test8930941/db-server/pkg/converters"
	"gitlab.com/test8930941/proto"
	"google.golang.org/protobuf/types/known/emptypb"
)

func (inst *Service) GetUser(ctx context.Context, req *proto.GetUserRequest) (*proto.GetUserResponse, error) {
	id, err := uuid.FromString(req.GetId())
	if err != nil {
		return nil, err
	}
	user, err := inst.repository.Get(id)
	if err != nil {
		return nil, err
	}
	response, err := converters.ConvertUserToProto(user)
	if err != nil {
		return nil, err
	}
	return &proto.GetUserResponse{User: response}, nil
}

func (inst *Service) GetUsersList(ctx context.Context, req *emptypb.Empty) (*proto.GetUsersListResponse, error) {
	users, err := inst.repository.List()
	if err != nil {
		return nil, err
	}
	response := new(proto.GetUsersListResponse)
	for _, user := range users {
		convertedUser, err := converters.ConvertUserToProto(user)
		// тут по-хорошему стек ошибок бы, но нам нужны все данные, поэтому падаем при любой проблеме
		if err != nil {
			return nil, err
		}
		response.Users = append(response.Users, convertedUser)
	}
	return response, nil
}

func (inst *Service) CreateUser(ctx context.Context, req *proto.CreateUserRequest) (*proto.CreateUserResponse, error) {
	creation, err := converters.ConvertUserCreationFromProto(req.GetUser())
	if err != nil {
		return nil, err
	}
	user, err := inst.repository.Create(*creation)
	if err != nil {
		return nil, err
	}
	response, err := converters.ConvertUserToProto(user)
	if err != nil {
		return nil, err
	}
	return &proto.CreateUserResponse{User: response}, err
}

func (inst *Service) DeleteUser(ctx context.Context, req *proto.DeleteUserRequest) (*emptypb.Empty, error) {
	id, err := uuid.FromString(req.GetId())
	if err != nil {
		return nil, err
	}
	//здесь можно логировать удаление
	_, err = inst.repository.Delete(id)
	if err != nil {
		return nil, err
	}
	return &emptypb.Empty{}, nil
}

func (inst *Service) ModifyUser(ctx context.Context, req *proto.ModifyUserRequest) (*proto.ModifyUserResponse, error) {
	modification, err := converters.ConvertUserModificationFromProto(req.GetUser())
	if err != nil {
		return nil, err
	}
	user, err := inst.repository.Modify(*modification)
	if err != nil {
		return nil, err
	}
	response, err := converters.ConvertUserToProto(user)
	if err != nil {
		return nil, err
	}
	return &proto.ModifyUserResponse{User: response}, err
}
