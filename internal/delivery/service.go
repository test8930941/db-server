package delivery

import (
	"log"
	"net"

	"gitlab.com/test8930941/db-server/internal/data"
	"gitlab.com/test8930941/proto"
	"google.golang.org/grpc"
)

type Service struct {
	address    string
	repository data.Repository
	Server     *grpc.Server
	proto.UserServiceServer
}

func NewService(address string, repository data.Repository) *Service {
	return &Service{
		address:    address,
		repository: repository,
	}
}

func (inst *Service) Run() error {
	listen, err := net.Listen("tcp", inst.address)
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	inst.Server = grpc.NewServer()
	proto.RegisterUserServiceServer(inst.Server, inst)

	log.Printf("gRPC service is listening on %s...\n", inst.address)
	return inst.Server.Serve(listen)
}

func (inst *Service) Stop() {
	if inst.Server != nil {
		inst.Server.GracefulStop()
	}
}
