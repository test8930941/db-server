package data

import (
	"github.com/gofrs/uuid"
	"gitlab.com/test8930941/db-server/internal/domain/entities"
)

type Repository interface {
	Get(uuid.UUID) (*entities.User, error)
	List() ([]*entities.User, error)
	Create(entities.UserCreation) (*entities.User, error)
	Modify(entities.UserModification) (*entities.User, error)
	Delete(uuid.UUID) (*entities.User, error) // возврат экземпляра для логов или аудита
}
