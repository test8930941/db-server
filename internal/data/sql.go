package data

import (
	"github.com/gofrs/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/test8930941/db-server/internal/domain/entities"
)

var (
	ErrIDGeneration = errors.New("id generation error")
)

type SQLRepository struct {
	db *sqlx.DB
}

func NewSQLRepository(db *sqlx.DB) *SQLRepository {
	return &SQLRepository{db: db}
}

func (inst *SQLRepository) Get(id uuid.UUID) (*entities.User, error) {
	out := new(entities.User)
	query := "SELECT * FROM users WHERE id = ?"
	if err := inst.db.Get(out, query, id); err != nil {
		return nil, err
	}
	return out, nil
}

func (inst *SQLRepository) List() ([]*entities.User, error) {
	out := make([]*entities.User, 0)
	// обработать бы лимит и оффсет, но это другая история с пагинацией
	query := "SELECT * FROM users"
	if err := inst.db.Select(&out, query); err != nil {
		return nil, err
	}
	return out, nil
}

func (inst *SQLRepository) Create(creation entities.UserCreation) (*entities.User, error) {
	id, err := uuid.NewV4()
	if err != nil {
		return nil, ErrIDGeneration
	}
	query := "INSERT INTO users (id, name, phone, birthday) VALUES (?, ?, ?, ?)"
	if _, err := inst.db.Exec(query, id, creation.Name, creation.Phone, creation.Birthday); err != nil {
		return nil, err
	}
	return &entities.User{
		ID:       id,
		Name:     creation.Name,
		Phone:    creation.Phone,
		Birthday: creation.Birthday,
	}, nil
}

func (inst *SQLRepository) Delete(id uuid.UUID) (*entities.User, error) {
	user, err := inst.Get(id)
	if err != nil {
		return nil, err
	}
	// возможно имеет смысл использовать транзакции
	query := "DELETE FROM users WHERE id = ?"
	_, err = inst.db.Exec(query, id)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (inst *SQLRepository) Modify(modification entities.UserModification) (*entities.User, error) {
	// на больших структурах возможно удобнее будет использовать NamedExec
	query := "UPDATE users SET name = ?, phone = ?, birthday = ? WHERE id = ?"
	_, err := inst.db.Exec(query, modification.Name, modification.Phone, modification.Birthday, modification.ID)
	if err != nil {
		return nil, err
	}
	return &entities.User{
		ID:       modification.ID,
		Name:     modification.Name,
		Phone:    modification.Phone,
		Birthday: modification.Birthday,
	}, nil
}
