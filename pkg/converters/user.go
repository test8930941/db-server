package converters

import (
	"time"

	"github.com/gofrs/uuid"
	"gitlab.com/test8930941/db-server/internal/domain/entities"
	"gitlab.com/test8930941/proto"
)

const DBDateLayout = "2006-01-02 15:04:05"

func ConvertUserToProto(user *entities.User) (*proto.User, error) {
	return &proto.User{
		Id:       user.ID.String(),
		Name:     user.Name,
		Phone:    user.Phone,
		Birthday: user.Birthday.Format(DBDateLayout),
	}, nil
}

func ConvertUserModificationFromProto(proto *proto.User) (*entities.UserModification, error) {
	id, err := uuid.FromString(proto.GetId())
	if err != nil {
		return nil, err
	}
	birthday, err := time.Parse(DBDateLayout, proto.GetBirthday())
	return &entities.UserModification{
		ID:       id,
		Name:     proto.GetName(),
		Phone:    proto.GetPhone(),
		Birthday: birthday,
	}, nil
}

func ConvertUserCreationFromProto(proto *proto.User) (*entities.UserCreation, error) {
	birthday, err := time.Parse(DBDateLayout, proto.GetBirthday())
	if err != nil {
		return nil, err
	}
	return &entities.UserCreation{
		Name:     proto.GetName(),
		Phone:    proto.GetPhone(),
		Birthday: birthday,
	}, nil
}
