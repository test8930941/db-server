#############################
# for more info see
# https://medium.com/@chemidy/create-the-smallest-and-secured-golang-docker-image-based-on-scratch-4752223b7324
############################

FROM golang:1.20-alpine AS builder

RUN apk update && apk add --no-cache ca-certificates tzdata
RUN update-ca-certificates
RUN adduser -D -g '' appuser

COPY . /app
RUN mkdir /app/bin

WORKDIR /app
RUN go mod vendor
RUN go build -o ./bin/apiserver ./cmd

FROM busybox

COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd

COPY --from=builder /app/bin /usr/bin/

USER appuser

EXPOSE 8500

ENTRYPOINT ["apiserver"]
